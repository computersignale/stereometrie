import { createApp } from 'vue'
import VueLazyload from '@jambonn/vue-lazyload'
import App from './App.vue'

document.settings = { "MEDIA_URL": process.env.NODE_ENV === 'production' ? '/thumbs' : process.env.VUE_APP_MEDIA_URL };

export default {
    app: createApp(App)
        .use(VueLazyload)
        .mount('#app')
}

